#ifndef LEDCUBEFRAME_H
#define LEDCUBEFRAME_H

#include <QColor>
#include <QObject>
#include <vector>

class LedCubeFrame {
  Q_GADGET
public:
  LedCubeFrame();
  ~LedCubeFrame();

  Q_INVOKABLE QColor getLed(int idx) const {
    Q_ASSERT(_frame && idx >= 0 && idx < static_cast<int>(_frame->size()));
    return (*_frame)[idx];
  }
  Q_INVOKABLE void setLed(int idx, QColor val);
  Q_INVOKABLE void clear();

  std::shared_ptr<std::vector<QColor>> frame() const { return _frame; };

private:
  std::shared_ptr<std::vector<QColor>> _frame;
};

#endif // LEDCUBEFRAME_H
