#include "ledcubegeometry.h"
#include <algorithm>
#include <QtMath>
#include <QVector3D>
#include <QVector2D>

LedCubeGeometry::LedCubeGeometry()
{
    updateData();
}

void LedCubeGeometry::setNumRows(int r)
{
    Q_ASSERT(r > 0);
    _size.y = r;

    updateData();
    update();
}

void LedCubeGeometry::setNumCols(int c)
{
    Q_ASSERT(c > 0);
    _size.x = c;

    updateData();
    update();
}

void LedCubeGeometry::setNumPlanes(int p)
{
    Q_ASSERT(p > 0);
    _size.z = p;

    updateData();
    update();
}

void LedCubeGeometry::updateData()
{
    // Constants
    static constexpr int stride = 3 * sizeof(float) + 2 * sizeof(float);
    static constexpr int numTrianglesPerLED = 10;
    static constexpr int numVerticesPerLED = numTrianglesPerLED * 3;
    int numLEDS = 2 * _size.x * _size.y * _size.z;
    int maxDimension = std::max(_size.x, std::max(_size.y, _size.z));
    float scale = 1.0f / maxDimension * 0.01;
    int vertexBufferSize = numLEDS * numVerticesPerLED * stride;

    // Create the vertex data buffer.
    QByteArray vertexData(vertexBufferSize, Qt::Uninitialized);
    float *posData = reinterpret_cast<float *>(vertexData.data() + 0);
    float *uvData = reinterpret_cast<float *>(vertexData.data() + 3 * sizeof(float));

    // Define helper functions.
    [[maybe_unused]] int ledsAdded = 0; // Safety check
    auto createLED = [&](QVector3D position, bool back, QVector2D uv){
        static constexpr QVector3D BBL(0, -0.3f, 0);
        static constexpr QVector3D BBR(0, 0.3f, 0);
        static constexpr QVector3D BFL(0.3f, -0.3f, 0);
        static constexpr QVector3D BFR(0.3f, 0.3f, 0);
        static constexpr QVector3D TBL(0, -0.3f, 0.6f);
        static constexpr QVector3D TBR(0, 0.3f, 0.6f);
        static constexpr QVector3D TFL(0.3f, -0.3f, 0.6f);
        static constexpr QVector3D TFR(0.3f, 0.3f, 0.6f);
        auto t = [&](QVector3D x){ return ((back ? QVector3D(-x.x(), -x.y(), x.z()) : x) + position) * scale; };
        auto set = [&](QVector3D pos, QVector2D uv)  {
            auto tp = t(pos);
            posData[0] = tp.x();
            posData[1] = tp.y();
            posData[2] = tp.z();
            uvData[0] = uv.x();
            uvData[1] = uv.y();
            posData = reinterpret_cast<float *>(reinterpret_cast<char *>(posData) + stride);
            uvData = reinterpret_cast<float *>(reinterpret_cast<char *>(uvData) + stride);
        };
        Q_ASSERT(ledsAdded < numLEDS);

        // Triangle 1
        set(TBL, uv);
        set(TBR, uv);
        set(TFR, uv);
        // Triangle 2
        set(TBL, uv);
        set(TFR, uv);
        set(TFL, uv);
        // Triangle 3
        set(TFR, uv);
        set(TBR, uv);
        set(BBR, uv);
        // Triangle 4
        set(TFR, uv);
        set(BBR, uv);
        set(BFR, uv);
        // Triangle 5
        set(TFL, uv);
        set(TFR, uv);
        set(BFR, uv);
        // Triangle 6
        set(TFL, uv);
        set(BFR, uv);
        set(BFL, uv);
        // Triangle 7
        set(TBL, uv);
        set(TFL, uv);
        set(BFL, uv);
        // Triangle 8
        set(TBL, uv);
        set(BFL, uv);
        set(BBL, uv);
        // Triangle 9
        set(BFL, uv);
        set(BFR, uv);
        set(BBR, uv);
        // Triangle 10
        set(BFL, uv);
        set(BBR, uv);
        set(BBL, uv);

        ledsAdded++;
    };

    auto createPair = [&](int r, int c, int p) {
        QVector3D position(-(_size.x - 1.0f) / 2 + c, -(_size.y - 1.0f) / 2 + r, p);
        QVector2D uvf((0.5f + c*2 ) / (_size.x*2), (0.5f + r + p * _size.y)/(_size.z * _size.y));
        QVector2D uvb((0.5f + c*2 + 1 ) / (_size.x*2), (0.5f + r + p * _size.y)/(_size.z * _size.y));
        auto offset = QVector3D(0.1f, 0, 0);
        createLED(position+offset, false, uvf);
        createLED(position-offset, true, uvb);
    };

    // Update the data
    // Clear the old data
    clear();
    for(int p = 0; p < _size.z; p++) {
        for(int r = 0; r < _size.y; r++) {
            for(int c = 0; c < _size.x; c++) {
                createPair(r,c,p);
            }
        }
    }
    Q_ASSERT(reinterpret_cast<char *>(posData) - vertexData.data() == vertexData.size());
    posData = reinterpret_cast<float *>(vertexData.data());

    setVertexData(vertexData);
    setStride(stride);
    setPrimitiveType(PrimitiveType::Triangles);

    addAttribute(Attribute::PositionSemantic, 0, Attribute::F32Type);
    addAttribute(Attribute::TexCoordSemantic, 3 * sizeof(float), Attribute::F32Type);
}



#include <QRandomGenerator>
ExamplePointGeometry::ExamplePointGeometry()
{
    updateData();
}

void ExamplePointGeometry::updateData()
{
    clear();

    constexpr int NUM_POINTS = 20000;
    constexpr int stride = 3 * sizeof(float);

    QByteArray vertexData;
    vertexData.resize(NUM_POINTS * stride);
    float *p = reinterpret_cast<float *>(vertexData.data());

    for (int i = 0; i < NUM_POINTS; ++i) {
        const float x = float(QRandomGenerator::global()->bounded(200.0f) - 100.0f) / 20.0f;
        const float y = float(QRandomGenerator::global()->bounded(200.0f) - 100.0f) / 20.0f;
        *p++ = x;
        *p++ = y;
        *p++ = 0.0f;
    }

    setVertexData(vertexData);
    setStride(stride);

    setPrimitiveType(QQuick3DGeometry::PrimitiveType::Points);

    addAttribute(QQuick3DGeometry::Attribute::PositionSemantic,
                 0,
                 QQuick3DGeometry::Attribute::F32Type);
}
