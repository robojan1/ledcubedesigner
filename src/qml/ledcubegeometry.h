#pragma once

#include <QQuick3DGeometry>
#include <size3d.h>

class LedCubeGeometry: public QQuick3DGeometry
{
    Q_OBJECT
    QML_NAMED_ELEMENT(LedCubeGeometry)
    Q_PROPERTY(int numRows READ numRows WRITE setNumRows)
    Q_PROPERTY(int numCols READ numCols WRITE setNumCols)
    Q_PROPERTY(int numPlanes READ numPlanes WRITE setNumPlanes)

public:
    LedCubeGeometry();

    void setNumRows(int r);
    void setNumCols(int c);
    void setNumPlanes(int p);

    int numRows() const { return _size.y; }
    int numCols() const { return _size.x; }
    int numPlanes() const { return _size.z; }

private:
    void updateData();

    Size3D _size { 1,1,1};

};

class ExamplePointGeometry : public QQuick3DGeometry
{
    Q_OBJECT
    QML_NAMED_ELEMENT(ExamplePointGeometry)

public:
    ExamplePointGeometry();

private:
    void updateData();
};

