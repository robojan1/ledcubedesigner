#ifndef LEDCUBEDESIGNER_H
#define LEDCUBEDESIGNER_H

#include <QApplication>
#include <QQmlApplicationEngine>
#include <appstate.h>

class LedCubeDesigner {
public:
  LedCubeDesigner(int argc, char *argv[]);
  ~LedCubeDesigner();

  int exec();

private:
  QApplication _app;
  QQmlApplicationEngine _engine;
  AppState _state;
};

#endif // LEDCUBEDESIGNER_H
