#ifndef LEDCUBELINEARMODEL_H
#define LEDCUBELINEARMODEL_H

#include <QAbstractListModel>
#include <QColor>
#include <QQmlEngine>
#include <memory>

class LedCubeLinearModel : public QAbstractListModel {
  Q_OBJECT
  QML_ANONYMOUS
  QML_UNCREATABLE("Should only be created on the c++ side")
public:
  explicit LedCubeLinearModel(QObject *parent = nullptr);

  // Basic functionality:
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;

  QVariant data(const QModelIndex &index,
                int role = Qt::DisplayRole) const override;

  // Editable:
  bool setData(const QModelIndex &index, const QVariant &value,
               int role = Qt::EditRole) override;

  Qt::ItemFlags flags(const QModelIndex &index) const override;

  Q_INVOKABLE QColor getLed(int idx) const {
    Q_ASSERT(_data && idx >= 0 && idx < static_cast<int>(_data->size()));
    return (*_data)[idx];
  }
  Q_INVOKABLE void setLed(int idx, QColor val);

  void setFullFrame(std::shared_ptr<std::vector<QColor>> data);

private:
  std::shared_ptr<std::vector<QColor>> _data;
};

#endif // LEDCUBELINEARMODEL_H
