#include "ledcubelinearmodel.h"

LedCubeLinearModel::LedCubeLinearModel(QObject *parent)
    : QAbstractListModel(parent) {}

int LedCubeLinearModel::rowCount(const QModelIndex &parent) const {
  // For list models only the root node (an invalid parent) should return the
  // list's size. For all other (valid) parents, rowCount() should return 0 so
  // that it does not become a tree model.
  if (!_data || parent.isValid())
    return 0;

  return static_cast<int>(_data->size());
}

QVariant LedCubeLinearModel::data(const QModelIndex &index, int role) const {
  if (!_data || !index.isValid())
    return QVariant();

  if (role != Qt::DisplayRole)
    return QVariant();

  int row = index.row();
  if (row >= static_cast<int>(_data->size()) || row < 0)
    return QVariant();

  return (*_data)[row];
}

bool LedCubeLinearModel::setData(const QModelIndex &index,
                                 const QVariant &value, int role) {
  if (!_data || !index.isValid() || role != Qt::DisplayRole)
    return false;

  int row = index.row();
  if (row >= static_cast<int>(_data->size()) || row < 0)
    return false;
  if (!value.canConvert<QColor>())
    return false;

  if (data(index, role) != value) {
    (*_data)[row] = qvariant_cast<QColor>(value);
    emit dataChanged(index, index, QVector<int>() << role);
    return true;
  }
  return false;
}

Qt::ItemFlags LedCubeLinearModel::flags(const QModelIndex &index) const {
  if (!index.isValid())
    return Qt::NoItemFlags;

  return Qt::ItemIsEditable;
}

void LedCubeLinearModel::setLed(int idx, QColor val) {
  Q_ASSERT(_data && idx >= 0 && idx < static_cast<int>(_data->size()));

  (*_data)[idx] = val;
  auto mdlIndex = index(idx);
  emit dataChanged(mdlIndex, mdlIndex, QVector<int>() << Qt::DisplayRole);
}

void LedCubeLinearModel::setFullFrame(
    std::shared_ptr<std::vector<QColor>> data) {
  if (data.get() == _data.get())
    return;
  beginResetModel();
  _data = std::move(data);
  endResetModel();
}
