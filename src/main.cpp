#include <ledcubedesigner.h>

int main(int argc, char *argv[])
{
    LedCubeDesigner app(argc, argv);

    return app.exec();
}
