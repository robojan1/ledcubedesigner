#include "appstate.h"

AppState::AppState(QObject *parent) : QObject(parent) {
  _frames.push_back({});
  _activeFrame = 0;
  _previewLedCube.setFullFrame(_frames[_activeFrame].frame());
}

void AppState::setActiveFrame(int frame) {
  if (frame == _activeFrame)
    return;

  if (frame < 0 || frame >= _frames.size())
    return;

  _activeFrame = frame;
  _previewLedCube.setFullFrame(_frames[_activeFrame].frame());
  emit activeFrameChanged();
  emit previewLedCubeChanged();
}

void AppState::addFrame(int insertPos) {
  _frames.insert(insertPos, {});
  if (insertPos <= _activeFrame) {
    _activeFrame++;
    emit activeFrameChanged();
  }
  emit numFramesChanged();
}

void AppState::removeFrame(int pos) {
  if (numFrames() == 1 || pos < 0 || pos >= numFrames())
    return;
  _frames.removeAt(pos);
  if (pos == _activeFrame || _activeFrame >= numFrames()) {
    if (_activeFrame >= numFrames()) {
      _activeFrame = numFrames() - 1;
    }
    _previewLedCube.setFullFrame(_frames[_activeFrame].frame());
    emit activeFrameChanged();
    emit previewLedCubeChanged();
  }
  emit numFramesChanged();
}
