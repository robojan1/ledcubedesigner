#include "ledcubeframe.h"
#include <algorithm>
#include <utility>

LedCubeFrame::LedCubeFrame() {
  _frame = std::make_shared<std::vector<QColor>>(10 * 10 * 10);
  clear();
}

LedCubeFrame::~LedCubeFrame() {}

void LedCubeFrame::clear() {
  Q_ASSERT(_frame);
  std::fill(_frame->begin(), _frame->end(), Qt::black);
}

void LedCubeFrame::setLed(int idx, QColor val) {
  Q_ASSERT(_frame && idx >= 0 && idx < static_cast<int>(_frame->size()));

  (*_frame)[idx] = val;
}
