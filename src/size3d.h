#ifndef SIZE3D_H
#define SIZE3D_H

#include <QObject>
#include <QQmlEngine>

class Size3D
{
    Q_GADGET
    QML_ELEMENT
    Q_PROPERTY(int sizeX MEMBER x)
    Q_PROPERTY(int sizeY MEMBER y)
    Q_PROPERTY(int sizeZ MEMBER z)
public:
    Size3D(int x, int y, int z);
    Size3D() = default;

    int x = 0;
    int y = 0;
    int z = 0;
};
Q_DECLARE_METATYPE(Size3D)

#endif // SIZE3D_H
