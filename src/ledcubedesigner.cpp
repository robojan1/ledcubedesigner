#include "ledcubedesigner.h"
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <qml/ledcubegeometry.h>
#include <size3d.h>

LedCubeDesigner::LedCubeDesigner(int argc, char *argv[]) : _app(argc, argv) {
  const QUrl mainPageUrl{QStringLiteral("qrc:/main.qml")};

  qmlRegisterType<LedCubeGeometry>("LedCubeDesigner", 1, 0, "LedCubeGeometry");
  qmlRegisterType<ExamplePointGeometry>("LedCubeDesigner", 1, 0, "ExamplePointGeometry");
  qmlRegisterType<LedCubeGeometry>("LedCubeDesigner", 1, 0, "Size3D");

  // Register objects
  _engine.rootContext()->setContextProperty("appstate", &_state);
  _engine.rootContext()->setContextProperty("previewLedCube",
                                            _state.previewLedCubeQml());

  QObject::connect(
      &_engine, &QQmlApplicationEngine::objectCreated, &_app,
      [mainPageUrl](QObject *obj, const QUrl &objUrl) {
        if (!obj && mainPageUrl == objUrl)
          QCoreApplication::exit(-1);
      },
      Qt::QueuedConnection);

  _engine.load(mainPageUrl);
}

int LedCubeDesigner::exec() { return _app.exec(); }

LedCubeDesigner::~LedCubeDesigner() {}
