target_sources(LedCubeDesigner PRIVATE
    ${CMAKE_CURRENT_LIST_DIR}/main.cpp
    ${CMAKE_CURRENT_LIST_DIR}/ledcubedesigner.cpp
    ${CMAKE_CURRENT_LIST_DIR}/ledcubedesigner.h
    ${CMAKE_CURRENT_LIST_DIR}/appstate.cpp
    ${CMAKE_CURRENT_LIST_DIR}/appstate.h
    ${CMAKE_CURRENT_LIST_DIR}/ledcubemodel.cpp
    ${CMAKE_CURRENT_LIST_DIR}/ledcubemodel.h
    ${CMAKE_CURRENT_LIST_DIR}/ledcubelinearmodel.cpp
    ${CMAKE_CURRENT_LIST_DIR}/ledcubelinearmodel.h
    ${CMAKE_CURRENT_LIST_DIR}/ledcubeframe.cpp
    ${CMAKE_CURRENT_LIST_DIR}/ledcubeframe.h
    ${CMAKE_CURRENT_LIST_DIR}/size3d.cpp
    ${CMAKE_CURRENT_LIST_DIR}/size3d.h)

target_include_directories(LedCubeDesigner PRIVATE ${CMAKE_CURRENT_LIST_DIR})

include(${CMAKE_CURRENT_LIST_DIR}/qml/CMakeLists.txt)
