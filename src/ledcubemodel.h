#ifndef LEDCUBEMODEL_H
#define LEDCUBEMODEL_H

#include <vector>
#include <QAbstractItemModel>
#include <QColor>

class LedCubeModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    explicit LedCubeModel(QObject *parent = nullptr,
                          int nColumns = 10,
                          int nRows = 10,
                          int nPlanes = 10);

    // Basic functionality:
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex &index) const override;

private:
    int _nColumns;
    int _nRows;
    int _nPlanes;
    std::vector<QColor> _data;
    size_t _planeStride;
    size_t _rowStride;
    size_t _columnStride;
};

#endif // LEDCUBEMODEL_H
