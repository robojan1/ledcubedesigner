#ifndef APPSTATE_H
#define APPSTATE_H

#include <QColor>
#include <QList>
#include <QObject>
#include <ledcubeframe.h>
#include <ledcubelinearmodel.h>

class AppState : public QObject {
  Q_OBJECT
  Q_PROPERTY(LedCubeLinearModel *previewLedCube READ previewLedCubeQml NOTIFY
                 previewLedCubeChanged)
  Q_PROPERTY(int activeFrame READ activeFrame WRITE setActiveFrame NOTIFY
                 activeFrameChanged)
  Q_PROPERTY(int numFrames READ numFrames NOTIFY numFramesChanged)

public:
  explicit AppState(QObject *parent = nullptr);

  inline LedCubeLinearModel *previewLedCubeQml() { return &_previewLedCube; }
  int activeFrame() const { return _activeFrame; }
  int numFrames() const { return _frames.size(); }

  void setActiveFrame(int frame);

  Q_INVOKABLE void addFrame(int insertPos);
  Q_INVOKABLE void removeFrame(int pos);

signals:
  void previewLedCubeChanged();
  void activeFrameChanged();
  void numFramesChanged();

private:
  LedCubeLinearModel _previewLedCube;
  QList<LedCubeFrame> _frames;
  int _activeFrame = 0;
};

#endif // APPSTATE_H
