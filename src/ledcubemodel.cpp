#include "ledcubemodel.h"

LedCubeModel::LedCubeModel(QObject *parent, int nColumns, int nRows, int nPlanes)
    : QAbstractItemModel(parent)
    , _nColumns(nColumns)
    , _nRows(nRows)
    , _nPlanes(nPlanes)
    , _planeStride(_nRows * _nColumns)
    , _rowStride(_nColumns)
    , _columnStride(1)
{}

QModelIndex LedCubeModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!parent.isValid()) {
        if (row < 0 || row >= _nPlanes)
            return QModelIndex();
        // Create index to plane layers
        return createIndex(row, 0, 1);
    } else {
        if (parent.internalId() == 1) {
            // Plane index
            int planeId = parent.row();
            Q_ASSERT(planeId >= 0 && planeId < _nPlanes);
            if (row < 0 || row >= _nRows || column < 0 || column >= _nColumns)
                return QModelIndex();
            return createIndex(row, column, planeId + 2);
        } else {
            // LED index not possible
            return QModelIndex();
        }
    }
}

QModelIndex LedCubeModel::parent(const QModelIndex &index) const
{
    // Root objects don't have a parent
    if (!index.isValid())
        return QModelIndex();
    int planeId = index.internalId();
    if (planeId == 1) {
        // Parent of the plane index is the root index
        return QModelIndex();
    } else {
        // LED index
        return createIndex(planeId - 2, 0, 1);
    }
}

int LedCubeModel::rowCount(const QModelIndex &parent) const
{
    // Root index returns the number of planes
    if (!parent.isValid())
        return _nPlanes;

    int planeId = parent.internalId();
    if (planeId == 1) {
        // The plane contains nRows number of rows
        return _nRows;
    } else {
        // The LED does not contain any more rows
        return 0;
    }
}

int LedCubeModel::columnCount(const QModelIndex &parent) const
{
    // The root index only has one column. Where each row contains the planes
    if (!parent.isValid())
        return 1;

    int planeId = parent.internalId();
    if (planeId == 1) {
        // The plane contains nColumns number of columns
        return _nColumns;
    } else {
        // The LED does not contain any columns
        return 0;
    }
}

QVariant LedCubeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole) {
        return QVariant();
    }

    int planeId = index.internalId();
    if (planeId == 1) {
        return QVariant();
    } else {
        int plane = planeId - 2;
        int row = index.row();
        int column = index.column();
        Q_ASSERT(plane >= 0 && plane < _nPlanes && row >= 0 && row < _nRows && column >= 0
                 && column < _nColumns);
        return _data[plane * _planeStride + column * _columnStride + row * _rowStride];
    }
}

bool LedCubeModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (Qt::DisplayRole != role || !index.isValid())
        return false;

    if (data(index, role) != value) {
        int planeId = index.internalId();
        if (planeId == 1) {
            return false;
        }

        int plane = planeId - 2;
        int row = index.row();
        int column = index.column();
        Q_ASSERT(plane >= 0 && plane < _nPlanes && row >= 0 && row < _nRows && column >= 0
                 && column < _nColumns);
        _data[plane * _planeStride + column * _columnStride + row * _rowStride]
            = qvariant_cast<QColor>(value);
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags LedCubeModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    int planeId = index.internalId();
    if (planeId == 1) {
        return Qt::NoItemFlags;
    } else {
        return Qt::ItemIsEditable;
    }
}
