import QtQuick 2.0
import QtQuick3D 1.15
import LedCubeDesigner 1.0

Node {
    id: cube
    property var model
    property var position

    Model {
        geometry: LedCubeGeometry {
            numCols: 2
            numRows: 2
            numPlanes: 2
        }
        materials: DefaultMaterial{
            emissiveColor: "white"
            diffuseColor: "white"
            emissiveFactor: 1.0
        }
        position: cube.position

        scale: Qt.vector3d(0.1,0.1,0.1)
    }

    Model {
        source: "#Cube"
        scale: Qt.vector3d(0.01,0.01,0.01);
        materials: DefaultMaterial{
            emissiveColor: "red"
            diffuseColor: "red"
            emissiveFactor: 1.0
        }
        position: Qt.vector3d(0, -1, 0)
    }

    /*
    Repeater3D {
        model: cube.model
        Node {
            id: led
            readonly property int column: index % 10
            readonly property int row: (index / 10) % 10
            readonly property int plane: (index / 100) % 10

            readonly property var scale: Qt.vector3d(0.002, 0.002, 0.00015)
            readonly property var position: cube.position.plus(
                                                Qt.vector3d(column, plane, 9-row))

            readonly property var frontColor: model.display
            readonly property var backColor: model.display

            // Front
            Model {
                source: "#Cube"
                scale: led.scale
                position: led.position.plus(Qt.vector3d(0, 0, 0.01))
                materials: DefaultMaterial {
                    emissiveColor: frontColor
                    diffuseColor: frontColor
                    emissiveFactor: 1.0
                }
            }

            // Back
            Model {
                source: "#Cube"
                scale: led.scale
                position: led.position.plus(Qt.vector3d(0, 0, -0.01))
                eulerRotation.y: 180
                materials: DefaultMaterial {
                    emissiveColor: backColor
                    diffuseColor: backColor
                    emissiveFactor: 1.0
                }
            }
        }
    }*/
}
