import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15

Window {
    width: 720
    height: 480
    visible: true
    title: qsTr("LED Cube Designer")
    EditingPane {
        id: editingPane
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.leftMargin: 0
        anchors.bottomMargin: 0
        anchors.topMargin: 0
        width: 250

        model: previewLedCube
    }

    CubePreview {
        model: previewLedCube
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: editingPane.right
    }
}
