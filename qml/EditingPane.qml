import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import Qt.labs.platform 1.1
import QtQuick.Layouts 1.12

Pane {
    id: editingPane
    required property var model

    property int activePalette: 0
    property var colorPalette: ["#000000", "#FFFFFF", "#FF0000", "#00FF00", "#0000FF", "#FF00FF", "#FFFF00", "#00FFFF", "#FF6600", "#66FF00",
        "#996600", "#669900", "#ff0099", "#0000CC", "#00CC00", "#404040", "#808080", "#C0C0C0", "#009933", "#CCFF00",
        "#000000", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000", "#000000"]
    readonly property var activeColor: colorPalette[activePalette]

    ColorDialog {
        id: colorDialog
        onAccepted: {
            colorPalette[activePalette] = color
            paletteRepeater.itemAt(activePalette).color = color
        }
    }

    Column {
        spacing: 5
        width: parent.width
        GroupBox {
            title: "Plane editor"
            anchors.left: parent.left
            anchors.right: parent.right
            Row {
                id: row
                PlaneEditor {
                    id: editor
                    model: editingPane.model
                    plane: planeSpinBox.value
                    editingColor: editingPane.activeColor
                }
                VerticalSpinBox {
                    id: planeSpinBox
                    from: 0
                    to: 9
                    width: 30
                    anchors.verticalCenter: parent.verticalCenter
                }
            }
        }

        GroupBox {
            title: "Color palette"
            GridLayout {
                columns: 10
                Repeater {
                    model: editingPane.colorPalette
                    id: paletteRepeater

                    Rectangle {
                        color: modelData
                        width: 13
                        height: 13
                        border.color: color.hslLightness > 0.5 ? "#000000" : "#CCCCCC"
                        border.width: index == editingPane.activePalette ? 2 : 0;
                        radius: 2
                        MouseArea {
                            acceptedButtons: Qt.LeftButton | Qt.MiddleButton
                            anchors.fill: parent
                            onClicked: {
                                editingPane.activePalette = index
                                if(mouse.button === Qt.MiddleButton) {
                                    colorDialog.open();
                                }
                            }
                        }
                    }
                }
            }
        }

        Pane {
            height: 40
            Row {
                height: parent.height
                Button {
                    text: "-"
                    width: 30
                    onClicked: appstate.removeFrame(appstate.activeFrame);
                    height: parent.height
                }
                Button {
                    text: "<"
                    width: 30
                    onClicked: appstate.activeFrame--
                    height: parent.height
                }
                Text {
                    text: "%1/%2".arg(appstate.activeFrame+1).arg(appstate.numFrames)
                    width: 30
                    horizontalAlignment: Qt.AlignHCenter
                    verticalAlignment: Qt.AlignVCenter
                    height: parent.height
                }
                Button {
                    text: ">"
                    width: 30
                    onClicked: appstate.activeFrame++
                    height: parent.height
                }
                Button {
                    text: "+"
                    width: 30
                    onClicked: appstate.addFrame(appstate.activeFrame+1);
                    height: parent.height
                }
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;height:250;width:150}
}
##^##*/
