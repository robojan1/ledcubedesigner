import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.15

Rectangle {
    id: control

    property var model
    property int plane
    property var editingColor: "black"

    color: "grey"

    implicitWidth: 150
    implicitHeight: 150

    GridLayout {
        id: editingGrid
        columns: 10
        anchors.fill: control
        columnSpacing: 5
        rowSpacing: columnSpacing

        Repeater {
            id: editorRepeater
            model: 10
            Repeater {
                model: 10
                property int rowIndex: 9 - index

                Rectangle {

                    property int columnIndex: index

                    property int modelIndex: control.plane * 100 + rowIndex * 10 + columnIndex
                    QtObject {
                        id: p
                        property int updateCounter: 0
                    }

                    function refresh() {
                        p.updateCounter++
                    }

                    width: 10
                    height: 10
                    color: p.updateCounter, control.model.getLed(modelIndex)
                    border.color: "black"
                    border.width: 1
                }
            }
        }
    }

    MouseArea {
        anchors.fill: editingGrid
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        hoverEnabled: true
        propagateComposedEvents: true

        function updateMouse(mouse) {
            var c = editingGrid.childAt(mouse.x, mouse.y)
            if (c && pressed) {
                var modelIndex = c.modelIndex
                if (mouse.buttons & Qt.LeftButton) {
                    console.log("Clicked: " + modelIndex)
                    control.model.setLed(modelIndex, control.editingColor)
                } else if (mouse.buttons & Qt.RightButton) {
                    control.model.setLed(modelIndex, "#000000")
                }
                c.refresh()
            }
        }

        onPressed: updateMouse(mouse)
        onPositionChanged: updateMouse(mouse)
    }
}

/*##^##
Designer {
    D{i:0;height:200;width:200}
}
##^##*/

