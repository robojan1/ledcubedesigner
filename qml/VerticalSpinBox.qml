import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15

SpinBox {
    id: control
    background: Rectangle {
        implicitWidth: 30
        border.color: "#bdbebf"
    }

    contentItem: Text {
        y: control.height / 3
        width: parent.width
        height: parent.height / 3
        text: control.textFromValue(control.value, control.locale)
        font: control.font
        horizontalAlignment: Qt.AlignHCenter
        verticalAlignment: Qt.AlignVCenter

    }
    up.indicator: Rectangle {
        y: 0
        width: parent.width
        height: parent.height / 3
        implicitHeight: 30
        implicitWidth: 30
        color: control.up.pressed ? "#e4e4e4" : "#f6f6f6"
        border.color: "#999999"

        Text {
            text: "+"
            font.pixelSize: control.font.pixelSize * 2
            color: "#21be2b"
            anchors.fill: parent
            fontSizeMode: Text.Fit
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }
    down.indicator: Rectangle {
        y: control.height * 2 / 3
        width: parent.width
        height: parent.height / 3
        implicitHeight: 30
        implicitWidth: 30
        color: control.down.pressed ? "#e4e4e4" : "#f6f6f6"
        border.color: "#999999"

        Text {
            text: "-"
            font.pixelSize: control.font.pixelSize * 2
            color: "#21be2b"
            anchors.fill: parent
            fontSizeMode: Text.Fit
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }
}
