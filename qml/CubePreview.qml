import QtQuick 2.4
import QtQuick3D 1.15
import LedCubeDesigner 1.0

View3D {
    id: preview
    property var model

    implicitHeight: 400
    implicitWidth: 400

    environment: SceneEnvironment {
        backgroundMode: SceneEnvironment.Color
        clearColor: "grey"
    }


    PerspectiveCamera {
        id: cam
        property real phi: 1.3
        property real theta: 1.3
        property real zoom: 15

        position: Qt.vector3d(Math.sin(theta) * Math.cos(phi) * zoom, Math.cos(theta) * zoom + 6, Math.sin(theta) * Math.sin(phi) * zoom)
        clipNear: 1

        function updateLookat()
        {
            lookAt(Qt.vector3d(0,5,0));
        }

        Component.onCompleted: updateLookat();
    }

    Model {
        source: "#Rectangle"
        scale: Qt.vector3d(0.3,0.3,0.3);
        z: 0
        eulerRotation.x: -90
        materials: PrincipledMaterial {
            metalness: 0
            roughness: 0.5
            baseColor: "darkgrey"
        }
    }

    Model {
        visible: true
        scale: Qt.vector3d(0.01, 0.01, 0.01)
        geometry: ExamplePointGeometry { }
        materials: [
            DefaultMaterial {
                lighting: DefaultMaterial.NoLighting
                cullMode: DefaultMaterial.NoCulling
                diffuseColor: "yellow"
            }
        ]
    }

//    Model {
//        source: "#Cube"
//        scale: Qt.vector3d(0.1,0.1,0.1);
//        materials: DefaultMaterial{
//            emissiveColor: "red"
//            diffuseColor: "red"
//            emissiveFactor: 1.0
//        }
//        position: Qt.vector3d(0, 1, 0)

//    }

    PointLight {
        position: Qt.vector3d(20, 10, -10)
    }

    LedCube {
        position: Qt.vector3d(0, 1, 0)
        model: preview.model
    }

    MouseArea {
        QtObject {
            id: persistentState
            property var lastMouseX : 0
            property var lastMouseY : 0
            property var isDragging : false
        }

        anchors.fill: parent
        onWheel: {
            var newZoom = cam.zoom + wheel.angleDelta.y * 0.01;
            if(newZoom < 1) newZoom = 1;
            if(newZoom > 50) newZoom = 50;
            cam.zoom = newZoom;
            cam.updateLookat();
        }
        onPositionChanged: {
            if(mouse.buttons & Qt.LeftButton) {
                if(persistentState.isDragging) {
                    var dx = mouse.x - persistentState.lastMouseX;
                    var dy = mouse.y - persistentState.lastMouseY;
                    var newPhi = cam.phi + dx * 0.01;
                    var newTheta = Math.min(Math.PI/2, Math.max(0, cam.theta + dy * -0.01));
//                    console.log("position: " + newPhi + " " + newTheta);
                    cam.phi = newPhi;
                    cam.theta = newTheta;
                    cam.updateLookat();
                }
                persistentState.isDragging = true;
                persistentState.lastMouseX = mouse.x;
                persistentState.lastMouseY = mouse.y;
            }
        }
        onReleased: {
            if(mouse.button === Qt.LeftButton) {
                persistentState.isDragging = false;
            }
        }
    }
}
